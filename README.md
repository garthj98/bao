# Pao Factory TDD project 

This project is to explore the coding of a bread factory (pao).

We will cover topics includon :

- git, bitbucket
- markdown
- Agile/scrum principles
- TDD
- Unit test
- Functional programing 


Other principles

- seperation ofo concerns 
- dry code

### TDD & Unit Tests

Unit test, is a single test that tests a part of a function.
Serveral tests together will help ensure teh functionality of the program, reduce technical debt and keep it maintainable.

Generally code lives, entropy adds complexity and short cuts lead to technical debt that can kill a project.

Technical debt is the concept of taking shortcuts - like skipping documentation or not making unit tests - leading to unmanagable code. Other factors such as a time, peopel leaving, updates also add to technical debt.

**Test Driven Development** Is a way of developing code that is very Agile :) It's the lightest implementation of Agile - Ensures working code 

You make the test first, then you code the least amount of code to make the test pass.

### User Stories and tests

Good user stories come with a unit test. These are usually done by the `Three Amigos` `Devs` + `Testers` + `bussiness analyst`.

In our case it will just be us.

**User Story 1** As a bread maker, I want to provide `flour` and `water` and get out `dough`, else I want `not dough`. So that I can then bake bread.

**User Story 2** As a bread maker, I want to be able to take `dough` and use the `oven` to get `Pao`. Else i should get `not Pao`. So that i can make bread

**User Story 3** As a bread maker, I wasnt an option of `run_factory` that will take in `flour` and `water` and give me `Pao`, else give me `not Pao`. So I can make bread with one simple action.

[]