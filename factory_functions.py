# Define the functions (not for running or calling)

def make_dough(arg1, arg2):
    ingredients = [arg1, arg2]
    if 'flour' in ingredients and 'water' in ingredients:
        return 'dough'
    else:
        return 'not dough'

def make_bread(arg1, arg2):
    tools = [arg1, arg2]
    if 'dough' in tools and 'oven' in tools:
        return 'Pao'
    else:
        return 'not Pao'

def run_factory(arg1, arg2):
    if make_bread(make_dough(arg1, arg2), 'oven') == 'Pao' or make_bread('oven', make_dough(arg1, arg2)) == 'Pao':
        return 'Pao'
    else:
        return 'not Pao'