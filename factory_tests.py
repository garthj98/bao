# Import funtions and run tests 
from factory_functions import *

## What is a test? 
# An assertion, with an known outcome of a function, that comes back with True or False

# **User Story 1** As a bread maker, I want to provide `flour` and `water` to my `make_dough` function and get out `dough`, else I want `not dough`. So that I can then bake bread.

print('test 1.0 ')
print(make_dough('flour', 'water') == 'dough')

print('test 1.1: ')
print(make_dough('flourws', 'water') == 'not dough')

print('test 1.2: ')
print(make_dough('water', 'water') == 'not dough')

print('test 1.3: ')
print(make_dough('bricks', 'water') == 'not dough')

print('test 1.4 ')
print(make_dough('water', 'flour') == 'dough')

# **User Story 2** As a bread maker, I want to be able to take `dough` and use the `oven` to get `Pao`. Else i should get `not Pao`. So that i can make bread

print('test 2.0: ')
print(make_bread('dough', 'oven') == 'Pao')

print('test 2.1: ')
print(make_bread('oven', 'dough') == 'Pao')

print('test 2.1: ')
print(make_bread('water', 'oven') == 'not Pao')

# **User Story 3** As a bread maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `Pao`, else give me `not Pao`. So I can make bread with one simple action.

print('test 3.0: ')
print(run_factory('flour', 'water') == 'Pao')

print('test 3.1: ')
print(run_factory('water', 'water') == 'not Pao')

print('test 3.2: ')
print(run_factory('water', 'flour') == 'Pao')

print('test 3.3: ')
print(run_factory('water', 'oven') == 'not Pao')