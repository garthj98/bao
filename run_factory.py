# Import functions and run the factory (call functions)
from factory_functions import *

while True:

    user_in_flour = input('Do you have flour? ').lower().strip()
    user_in_water = input('Do you have water? ').lower().strip()
    user_in_oven = input('Do you have an oven? ').lower().strip()

    if user_in_flour == 'no':
        print('You are missing flour')
    if user_in_water == 'no':
        print('You are missing water')
    if user_in_oven == 'no':
        print('You are missing an oven')
    if user_in_flour == 'no' or user_in_water == 'no' or user_in_oven == 'no':
        exit()

    if user_in_flour == 'yes' and user_in_water == 'yes' and user_in_oven == 'yes':
        print('You can make ' + run_factory('flour', 'water'))
    else:
        print('Please ensure all of your responses to the previous questions are yes or no')

